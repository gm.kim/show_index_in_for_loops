# -*- coding: utf-8 -*-


import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))



from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5 import uic

import process_insert_code
import telegram
import re
import imp

class Thread(QThread):
    
    
    change_value = pyqtSignal(int)
    label_value = pyqtSignal(str)
    end_value = pyqtSignal(str)
    restart_value = pyqtSignal(str)
    
    
    
    
    def run(self):
        self.is_working = True
        self.complete_sign = False
        
        import convert_code_for_loop
        imp.reload(convert_code_for_loop)
        
        convert_code_for_loop.runpyfunction(self)
        if self.complete_sign:
            self.end_value.emit("완료")
        self.restart_value.emit("재시작")
            





form_class = uic.loadUiType("seminardesigner.ui")[0]

class MyWindow(QWidget,form_class):
    def __init__(self) :
        super().__init__()
        self.setupUi(self)
        self.is_running= False
        

        #button들의 시그널
        self.openButton.clicked.connect(self.openButtonClicked)
        self.runButton.clicked.connect(self.clickRun)
        self.stopButton.clicked.connect(self.threadStop)        
        
        
    def openButtonClicked(self):
        self.fname = QFileDialog.getOpenFileName(self)
        self.label.setText(self.fname[0])
        fname_list = str(self.fname[0]).split("\\")
        self.fname_no_name = ""
        for z in range(len(fname_list)-1):
            self.fname_no_name = self.fname_no_name + fname_list[z] + "\\"         
        self.new_fname = self.fname_no_name + "convert_code_for_loop.py"
        process_insert_code.file_processing_function(self.fname[0],self.new_fname)
        
        
        
        
#    def showMessageBox(self):
#        msg = QMessageBox()
#        msg.setWindowTitle("완료 알림")
#        msg.setText("완료되었습니다.")
#        msg.setStandardButtons(QMessageBox.Ok)
#        result = msg.exec_()
        
        
    def telegram_alarm(self):
        tokenfilefath = self.fname_no_name + "token.txt"
        text = open(tokenfilefath,'r',encoding='utf-8')
        token = text.readlines()
        pattern = "token = '(.*)'"
        r = re.compile(pattern)
        token = r.search(token[0]).group(1)
        bot = telegram.Bot(token=token)
        chat_id = bot.getUpdates()[-1].message.chat.id
        bot.sendMessage(chat_id = chat_id, text="완료되었습니다.")
#        os.remove(self.new_fname)


        
    def clickRun(self):
        if not self.is_running:
            self.th = Thread()
            self.th.change_value.connect(self.setValueGM)
            self.th.label_value.connect(self.setLabelValueGM)
#            self.th.end_value.connect(self.showMessageBox)
            self.th.end_value.connect(self.telegram_alarm)
            self.th.restart_value.connect(self.isrunningFalse)
            self.th.start()
            self.is_running = True
            

    def threadStop(self):
        self.th.is_working = False
        self.label.setText("")
        self.progressBar_gm.setValue(0)
        self.label_gm.setText("0 / 0")
#        os.remove(self.new_fname)
        
        
    def isrunningFalse(self):
        self.is_running= False
        
        

            
        
    def setValueGM(self, value):
        self.progressBar_gm.setValue(value)
        
        
        
    def setLabelValueGM(self, value):
        self.label_gm.setText(value)
        
        
        


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyWindow()
    window.show()
    app.exec_()
    
    
    
    