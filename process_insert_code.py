# -*- coding: utf-8 -*-

import re


def file_processing_function(filepath,newfilepath):
    
    
    text = open(filepath,'r',encoding='utf-8')
    lines = text.readlines()
    
    
    lines.insert(len(lines),"print("")")

    
    
    
    
    #주석은 code 실행 시 의미가 없고, code 파악을 방해하므로 사전 제거한다.
    pattern = '#(.*)'
    r = re.compile(pattern)
    
    
    comment_list = []
    for x in range(len(lines)):
        if r.search(lines[x]):
            comment_list.append(x)
            

    for x in comment_list:
        lines[x] = re.sub('#(.*)','',lines[x])



    
    lines.insert(0,"def runpyfunction(thread):\n")
#    lines.insert(1,"runpyfunction.now_for_loop = 0\n")
    lines.insert(1,"runpyfunction.process_num_sig = 0\n")
    lines.insert(2,"runpyfunction.index_now = 0\n")


    
    count = -1
    global total
    total = []
    x_list_1 = []
    x_list_2 = []
    x_list_3 = []
    x_list_4 = []

    
    
    
    
    # code 추가
    pattern = 'for(.+)in enumerate'
    r = re.compile(pattern)
    
    
    for x in range(len(lines)):
        if r.search(lines[x]):
            x_list_1.append(x)
           
            
    for x in x_list_1:
        count = count + 1
        x = x + (count*11)
        
            
        arr = lines[x].split()
        pattern2 = '^enumerate\((.+)\):'
        r = re.compile(pattern2)
        mo = r.search(arr[-1])
        val_1 = mo.group(1)
        total.append(val_1)
            
        pattern = '^(.*)for'
        r = re.compile(pattern)
        mo = r.search(lines[x])
        space_count = mo.group(1)


        lines.insert(x+1, space_count+"    if runpyfunction.process_num_sig == 1:\n")
        lines.insert(x+2, space_count+"        if thread.is_working == False:\n")
        lines.insert(x+3, space_count+"            break\n") 
        lines.insert(x+4, space_count+"        runpyfunction.index_now += 1\n")
        lines.insert(x+5, space_count+"        length = int(len(%s))\n" %total[count])
        lines.insert(x+6, space_count+"        thread.change_value.emit(int(((runpyfunction.index_now)/length)*100))\n")
        lines.insert(x+7, space_count+"        thread.label_value.emit(str(runpyfunction.index_now)+' / '+str(length))\n")
       
        lines.insert(x+8, space_count+"        if runpyfunction.index_now == length:\n")
        lines.insert(x+9, space_count+"            runpyfunction.index_now = 0\n")
#        lines.insert(x+10, space_count+"        thread.for_loop_value.emit(str(runpyfunction.now_for_loop+1) + ' / ' +str(count_for_loop))\n")

            
        lines.insert(x, space_count+"runpyfunction.process_num_sig += 1\n")
        
        
        pattern = '^( {0,}).'
        r = re.compile(pattern)

        for n in range(x+3,len(lines)):
            if r.search(lines[n]):
                mo = r.search(lines[n])
                space_count2 = mo.group(1)
                
                if len(space_count2) <= len(space_count):
                    finish_index = n
                    break

            
        try:   
            finish_index
        except NameError:
            pass
        else:
            lines.insert(finish_index, space_count+"runpyfunction.process_num_sig -= 1 \n")



        #return이 포함된 부분이 있는지 찾는다.(함수가 끝까지 돌지않고 끝나므로)
        pattern_return = '(.*)return (.+)'
        r = re.compile(pattern_return)
        
        return_index_list = []
        return_index_space_count = []
        for n in range(x,finish_index):
            if r.search(lines[n]):
                mo = r.search(lines[n])
                return_index_list.append(n)
                return_index_space_count.append(mo.group(1))
                
        for num,n in enumerate(return_index_list):
            lines.insert(n, return_index_space_count[num]+"runpyfunction.process_num_sig -= 1 \n")




        

            
            
            
            
        
        


            

        
    count = -1
    total = []
 
    pattern = 'for(.+)in(.*)\.iterrows'
    r = re.compile(pattern)
    
    for x in range(len(lines)):
        if r.search(lines[x]):
            x_list_2.append(x)
            
            
    for x in x_list_2:
        count = count + 1
        x = x + (count*11)
        
        pattern = 'for(.+)in(.*)\.iterrows'
        r = re.compile(pattern)

        
        arr = lines[x].split()
        arr.insert(1,"index,")
            
        mo = r.search(lines[x])
        val_1 = mo.group(2)
        val_1 = val_1.replace(" ","") 
        total.append(val_1)
            
        pattern = '^(.*)for'
        r = re.compile(pattern)
        mo = r.search(lines[x])
        space_count = mo.group(1)


        
        lines.insert(x+1, space_count+"    if runpyfunction.process_num_sig == 1:\n")
        lines.insert(x+2, space_count+"        if thread.is_working == False:\n")
        lines.insert(x+3, space_count+"            break\n") 
        lines.insert(x+4, space_count+"        runpyfunction.index_now += 1\n")
        lines.insert(x+5, space_count+"        length = int(len(%s))\n" %total[count])
        lines.insert(x+6, space_count+"        thread.change_value.emit(int(((runpyfunction.index_now)/length)*100))\n")
        lines.insert(x+7, space_count+"        thread.label_value.emit(str(runpyfunction.index_now)+' / '+str(length))\n")
        lines.insert(x+8, space_count+"        if runpyfunction.index_now == length:\n")
        lines.insert(x+9, space_count+"            runpyfunction.index_now = 0\n")

#        lines.insert(x+10, space_count+"        thread.for_loop_value.emit(str(runpyfunction.now_for_loop+1) + ' / ' +str(count_for_loop))\n")


        lines.insert(x, space_count+"runpyfunction.process_num_sig += 1\n")


        pattern = '^( {0,}).'
        r = re.compile(pattern)

        for n in range(x+3,len(lines)):
            if r.search(lines[n]):
                mo = r.search(lines[n])
                space_count2 = mo.group(1)
                
                if len(space_count2) <= len(space_count):
                    finish_index = n
                    break

        try:   
            finish_index
        except NameError:
            pass
        else:
            lines.insert(finish_index, space_count+"runpyfunction.process_num_sig-= 1 \n")
            
            
        pattern_return = '(.*)return (.+)'
        r = re.compile(pattern_return)
        
        return_index_list = []
        return_index_space_count = []
        for n in range(x,finish_index):
            if r.search(lines[n]):
                mo = r.search(lines[n])
                return_index_list.append(n)
                return_index_space_count.append(mo.group(1))
                
        for num,n in enumerate(return_index_list):
            lines.insert(n, return_index_space_count[num]+"runpyfunction.process_num_sig -= 1 \n")


            












        
            
 
            
    count = -1
    total = []

    pattern = 'for(.*)in((?!enumerate)(?!iterrows).)*:$'
    r = re.compile(pattern)

    for x in range(len(lines)):
        if r.search(lines[x]):
            x_list_3.append(x)
            

    for x in x_list_3:
        count = count + 1
        x = x + (count*11)
        
    
        arr = lines[x].split()

        val_1 = arr[-1].replace(":","")
        total.append(val_1)


        pattern = '^(.*)for'
        r = re.compile(pattern)
        mo = r.search(lines[x])
    
    
        try:
            mo.group(1)
        except AttributeError:
            
            x = x-1
            arr = lines[x].split()

            val_1 = arr[-1].replace(":","")
            del total[-1]
            total.append(val_1)
    
    
            pattern = '^(.*)for'
            r = re.compile(pattern)
            mo = r.search(lines[x])
            space_count = mo.group(1)

        else:
            space_count = mo.group(1)
    

            
        
        lines.insert(x+1, space_count+"    if runpyfunction.process_num_sig == 1:\n")
        lines.insert(x+2, space_count+"        if thread.is_working == False:\n")
        lines.insert(x+3, space_count+"            break\n") 
        lines.insert(x+4, space_count+"        runpyfunction.index_now += 1\n")
        lines.insert(x+5, space_count+"        length = int(len(%s))\n" %total[count])
        lines.insert(x+6, space_count+"        thread.change_value.emit(int(((runpyfunction.index_now)/length)*100))\n")
        lines.insert(x+7, space_count+"        thread.label_value.emit(str(runpyfunction.index_now)+' / '+str(length))\n")
        lines.insert(x+8, space_count+"        if runpyfunction.index_now == length:\n")
        lines.insert(x+9, space_count+"            runpyfunction.index_now = 0\n")

#        lines.insert(x+10, space_count+"        thread.for_loop_value.emit(str(runpyfunction.now_for_loop+1) + ' / ' +str(count_for_loop))\n")
        

        lines.insert(x, space_count+"runpyfunction.process_num_sig += 1\n")
    

        pattern = '^( {0,}).'
        r = re.compile(pattern)

        for n in range(x+3,len(lines)):
            if r.search(lines[n]):
                mo = r.search(lines[n])
                space_count2 = mo.group(1)
                
                if len(space_count2) <= len(space_count):
                    finish_index = n
                    break
                
            
        try:   
            finish_index
        except NameError:
            pass
        else:
            lines.insert(finish_index, space_count+"runpyfunction.process_num_sig-= 1 \n")
        
        
        pattern_return = '(.*)return (.+)'
        r = re.compile(pattern_return)
        
        return_index_list = []
        return_index_space_count = []
        for n in range(x,finish_index):
            if r.search(lines[n]):
                mo = r.search(lines[n])
                return_index_list.append(n)
                return_index_space_count.append(mo.group(1))
                
        for num,n in enumerate(return_index_list):
            lines.insert(n, return_index_space_count[num]+"runpyfunction.process_num_sig -= 1 \n")


    










        
        
    #마지막 코드에 complete sign signal을 줘야한다.  
    a = lines[len(lines)-1]
    lines[len(lines)-1] = a.replace(a,a+"\n")
    
    lines.insert(len(lines), "if thread.is_working == True:\n")
    lines.insert(len(lines), "    thread.complete_sign = True")


        
    for x in range(1,len(lines)):
        lines[x] = "    " + lines[x]

 
    text.close()
    
    


    text = open(newfilepath,'w',encoding='utf-8')
    text.writelines(lines)
    text.close()
    
    
    
    
    
    
    
    
    
    
    
    
    