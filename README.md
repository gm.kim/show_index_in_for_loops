# show index in 'for' loops


#### 하나의 file(.py)에 작성된 'for'문의 진행정도를 보여주고, code 완료 시 텔레그램 알림을 전송해주는 프로그램


## 사용 라이브러리
* pandas
* re
* telegram
* PyQt5



## 라이브러리 설치
* pip install python-telegram-bot
* pip install telegram


## 프로그램 사용 유의사항 (필독)
* python file에서 code가 끝나는 마지막 줄은 공란으로 남겨두어야합니다. (들여쓰기도 포함하지 말 것)

![예시1](/uploads/3064d4ccfd5b0b26b184251bc8e899ad/예시1.PNG) ![예시2](/uploads/c3dcb5fc4bb2f579fafae21b53d3ca9d/예시2.PNG)   
좌측이 올바른 예시입니다.


## 프로그램 작동 방식
1. 사용자는 open 버튼을 눌러 실행시키고 싶은 file(.py)을 불러올 수 있습니다.
2. 사용자가 파일을 불러옴과 동시에 for문의 구조를 파악해 반복문의 진행정도(현재 index) 넘겨줄 수 있는 code가 삽입됩니다.
3. code가 삽입되고난 뒤, 전체 code는 별도의 파일에 저장됩니다.(같은 디렉토리에 "convert_code_for_loop.py"로 저장됨)
4. run 버튼을 누르면 새로 저장된 파일이 실행됩니다.
5. stop 버튼을 누르면 사용자가 의도적인 종료를 할 수 있습니다.
6. 의도적인 종료를 제외한 정상 종료의 경우 완료 시, 사용자는 telegram을 통해 "완료되었습니다."라는 메세지를 받을 수 있습니다.



## 텔레그램 별도 설정
 
![botFather](/uploads/7ad977afaf20c031174546f20a64c49d/botFather.PNG)   
![borFather_start](/uploads/a6f2fdcb6996d691fe229012c61e6de3/borFather_start.PNG)   
* 1. BotFather에게 "/start" 라는 메세지를 전송합니다.

![기능](/uploads/2d764f382329c071f044e10da9477290/기능.PNG)   
* 2. BotFather은 사용 가능한 기능들을 알려줍니다.

![make_Bot](/uploads/b5571b5d8f928654c139a969e753365e/make_Bot.PNG)   
![num](/uploads/adfda70aed17fd1d6045b663492f51a9/num.PNG)   
* 3. 차례로 "/newbot" , "만들고싶은 bot의 이름" , "만들고싶은 bot의 이름 + _bot" 을 전송합니다.   
Use this token to access the HTTP API: 이후에 나오는 HTTP API를 잘 기억해 두어야 합니다.


![bot_made](/uploads/cf18300d806b0d600e47def6136a5dce/bot_made.PNG)   
* 4. bot이 정상적으로 생성되었습니다.


![token](/uploads/6faeac3813542eb1984ef5850cf8bf55/token.PNG)   
![token메모장](/uploads/5210ee701b8bb55bcee2b7640d2374f0/token메모장.PNG)   
* 5. token.txt 파일에 발급받은 HTTP API를 입력하고 저장하세요.





![API전송](/uploads/149fcf4d1313f4365986413617b07799/API전송.PNG)   
* 6. 생성한 bot의 대화창에 "/start" , "https://api.telegram.org/bot<위에서 기억한 HTTP API입력>/getUpdates"를 순서대로 전송합니다.   
(처음에 있는 /start 는 무시하고 전송하시면 됩니다.)

![새창](/uploads/411f2e4d7e010fdaf75b336113a871a3/새창.PNG)   
* 7. chat id가 발급된 것을 확인할 수 있습니다.


![재부팅](/uploads/9e555b961b0fddcfeb367f6604414ef8/재부팅.PNG)   
* 8. 재부팅 혹은 chat id 만료 시, 다음과 같이 재전송해주시면 다시 알림을 받을 수 있습니다.


## 실행
![초기화면](/uploads/b60cc7ccf6c206c9e9eb6ef22aa75166/초기화면.PNG)   
* 프로그램 실행 시 나오는 초기화면 입니다.


![파일불러오기2](/uploads/04e2016789dc13aa12694240db6afc3e/파일불러오기2.PNG)   
* "Open" 버튼을 눌러 파일을 불러올 수 있으며, 파일 directory가 함께 표시됩니다.

![run2](/uploads/175426dfaf699a9159096d42b2979ebf/run2.PNG)   
* "run" 버튼을 누르면 파일이 실행되며 progress bar와 "진행정도/전체반복횟수"가 표시됩니다.


![stop](/uploads/dd10e83a5c2550d9cf8f60e900ff93ac/stop.PNG)   
* "stop" 버튼을 눌러 의도적으로 실행을 중지시킬 수 있습니다.

![알림2](/uploads/0392e71e59b05091096521426998a445/알림2.PNG)   
* 실행이 모두 완료되면 "완료되었습니다." 라는 메세지를 받을 수 있습니다.







